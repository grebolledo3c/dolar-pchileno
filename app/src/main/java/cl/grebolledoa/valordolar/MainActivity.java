package cl.grebolledoa.valordolar;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static final int VALOR_DOLAR = 780;

    private EditText etValorIngresado;
    private TextView tvResultado;
    private TextView tvError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.etValorIngresado = findViewById(R.id.et_dolar_ingresado);
        this.tvResultado = findViewById(R.id.tv_resultado);
        this.tvError = findViewById(R.id.tv_error);
    }

    public void btConvertirOnClick(View v){
        try{
            if(this.etValorIngresado.getText().toString().isEmpty()){
                Toast.makeText(this,"Ingrese un valor", Toast.LENGTH_SHORT).show();
            } else {
                String textoValoringresado = this.etValorIngresado.getText().toString();
                int valorIngresado, resultado;

                valorIngresado  = Integer.parseInt(textoValoringresado);

                resultado = valorIngresado * VALOR_DOLAR;

                String textoResultado = getResources().getString(R.string.texto_resultado);

                textoResultado = textoResultado.replace("${valor_ingresado}", String.valueOf(valorIngresado));

                textoResultado = textoResultado.replace("${resultado}", String.valueOf(resultado));

                this.tvResultado.setText(textoResultado);
                this.tvError.setText("");
            }

        }catch (Exception ex){
            String textError = getResources().getString(R.string.error);
            String error = ex.getMessage() == null ? "error" : ex.getMessage();
            Log.e("Error", error);
            textError = textError.replace("${error}", error);
            this.tvError.setText(textError);
        }

    }
}